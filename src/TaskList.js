import React from "react";
import TaskAction from "./TaskAction";


function TasksList (props) {
  
    return (
      <div className="tasks-list-container">
        <div className="tasks-list">
          {props.tasks.map((task) => (
            <TaskAction
              text={task.text}
              id={task.id}
              removeTask={props.removeTask}
            />
          ))}
        </div>
      </div>
    );
  
}

export default TasksList;
