import React from "react";


function Task2 (props) {
 
const [backgroundColor,setBackgroundColor]=React.useState("")
 const  removeTask=()=> {
    props.removeTask(props.id);
  }

  const markDone=()=> {
    if (backgroundColor === "") {
      setBackgroundColor("green")
    } else {
      setBackgroundColor("")
    }
  }

  
    return (
      <div className="task-container">
        <div
          className="task-container-background"
          style={{ backgroundColor: backgroundColor }}
        >
          <div className="task-term-container">
            <h2 className="task-term">{props.text}</h2>
          </div>
          <div className="buttons-container">
            <button className="done-button" onClick={markDone}>
              ✓
            </button>
            <button className="delete-button" onClick={removeTask}>
              x
            </button>
          </div>
        </div>
      </div>
    );
  
}

export default Task2;
