import React from "react";
import SubmitForm from "./SubmitForm";
import TasksList from "./TaskList";

function TodoTask () {
 
  const [tasks,setTasks]=React.useState([])

  const addTask=(task)=> {
    // this.setState((prevState) => {
    //   return {
    //     tasks: prevState.tasks.concat(task),
    //   };
    // });
    setTasks(tasks.concat(task))
  }

  const removeTask=(id)=> {
    const taskDelete = tasks.filter((element) => element.id !== id);
    setTasks(taskDelete)
  }

 
    return (
      <div className="App">
        <SubmitForm tasks={tasks} addTask={addTask} />
        <TasksList tasks={tasks} removeTask={removeTask} />
      </div>
    );
  
}

export default TodoTask;
