import React from "react";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

class SubmitForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { tasksCount: 0 };

    this.addItem = this.addItem.bind(this);
  }

  addItem(e) {
    if (this.props.tasks.length === 6) {
      toast.error("Sorry You Cant Add Task");
      this._inputElement.value = "";
    } else if (this._inputElement.value !== "") {
      const newItem = {
        text: this._inputElement.value,
        id: this.state.tasksCount,
      };

      this.props.addTask(newItem);
      
      this.state.tasksCount++;
      this._inputElement.value = "";
    } else {
      return null;
    }

    e.preventDefault();
  }
  render() {
    return (
      <div className="form">
          <h1>Todo List</h1>
        <form onSubmit={this.addItem}>
          <input
            className="input"
            ref={(e) => (this._inputElement = e)}
            placeholder="Add Task"
          />
          <button className="add-button" type="submit">
            Add
          </button>
          <ToastContainer />
        </form>
      </div>
    );
  }
}
export default SubmitForm;
